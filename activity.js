db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc"}},
	{$count: "amountOfRedFarmItems"}

])

db.fruits.aggregate([

	{$match: {supplier:"Green Farming and Canning"}},
	{$count: "amountOfGreenFarmItems"}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", avgPrice: {$avg: "$price"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", maxPrice: {$max: "$price"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", minPrice: {$min: "$price"}}}

])	

